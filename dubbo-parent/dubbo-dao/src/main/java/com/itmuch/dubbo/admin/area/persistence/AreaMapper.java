package com.itmuch.dubbo.admin.area.persistence;

import com.itmuch.dubbo.admin.area.domain.Area;
import tk.mybatis.mapper.common.Mapper;

public interface AreaMapper extends Mapper<Area> {
}