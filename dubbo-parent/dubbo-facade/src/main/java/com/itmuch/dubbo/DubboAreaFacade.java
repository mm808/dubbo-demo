package com.itmuch.dubbo;

import com.itmuch.dubbo.admin.area.domain.Area;

public interface DubboAreaFacade {
    public Area selectById(Integer id);
}
