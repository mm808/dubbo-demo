package com.itmuch.platform.controller.admin;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itmuch.core.web.converter.Result;
import com.itmuch.dubbo.DubboAreaFacade;

@Controller
@RequestMapping(value = "/area")
public class AreaAdminController {

    @Resource
    private DubboAreaFacade dubboAreaFacade;

    @RequestMapping("/{id}")
    @ResponseBody
    public Result index(@PathVariable Integer id) {
        return new Result(this.dubboAreaFacade.selectById(id));
    }

}
