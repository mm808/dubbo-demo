package com.itmuch.dubbo;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.itmuch.dubbo.admin.area.domain.Area;
import com.itmuch.dubbo.admin.area.persistence.AreaMapper;

@Service("dubboAreaFacade")
public class AreaService implements DubboAreaFacade {
    @Resource
    private AreaMapper areaMapper;

    @Override
    public Area selectById(Integer id) {
        this.abc();
        return this.areaMapper.selectByPrimaryKey(id);
    }

    public void abc() {
        System.out.println(1);
    }
}
